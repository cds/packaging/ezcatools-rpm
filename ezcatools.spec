Name:       ezcatools
Summary:    Command line tools that use the ezca system to interact with EPICS process variables.
Version:    1.0.0
Source:     file:///ezcatools-prerelease.tar.gz
Release:    1%{?dist}
License:    GPL2+
BuildRequires: cmake3
BuildRequires: cds-crtools-devel >= 0.5.0~rc4
BuildRequires: gds-base-devel
BuildRequires: epics-extension-ezca
Obsoletes: gds-dtt-crtools <= 2.20
Obsoletes: cds-crtools <= 0.4.1

%description
%{summary}

%prep
%setup -q -n ezcatools

%build
%cmake3 -DGDS_INCLUDE_DIR:PATH=%{_includedir}/gds .
%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/*

%changelog
* Tue May 18 2021 Erik von Reis <evonreis@caltech.edu> - 1.0.0-1
- Initial package